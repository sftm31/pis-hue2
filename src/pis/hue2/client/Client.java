package pis.hue2.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.SwingUtilities;

import pis.hue2.client.gui.GUI;
import pis.hue2.common.Communicator;
import pis.hue2.common.Instruction;
import pis.hue2.common.Message;
import pis.hue2.common.SocketMechanics;

/**
 * Client object which manages a socket connection and a gui and is able to send and receive data from a server which it is connected to
 */
public class Client implements SocketMechanics, Communicator{
	
	/**
	 * The GUI component which the client is working on using the Event Dispatch Thread
	 */
	GUI g;
	
	/**
	 * The Socket which we will be using for the Communication with the Server
	 */
	Socket s;
	
	/**
	 * port of the client
	 */
	int port;
	
	/**
	 * for input , output , and the host name of the Client
	 */
	public String input, output, hostname;
	
	/**
	 * sends the Data form The Client to the Server
	 */
	OutputStreamWriter out;
	
	/**
	 * Reads a string message out of the input stream
	 * Waits for a message
	 */
	BufferedReader read;
	
	/**
	 * Writes messages to the client using the output stream
	 */
	PrintWriter send;
	
	/**
	 * sends the Data form The Client to the Server
	 */
	OutputStream os;
	
	public Message fileToSend;
	
	/**
	 * Constructor for the client object, capturing a hostname default: "localhost" and a port to which the client socket is trying to connect to a server socket
	 */
	public Client (String hostname, int port, GUI g) {
		this.hostname = hostname;
		this.port = port;
		this.g = g;
	}

	
	/**
	 * Starts the Client and is waiting for a connection.
	 */
	@Override
	public void start(){
		try {
			log("[Client] Trying connection.");
			s = new Socket(hostname, port);
			
			out = new OutputStreamWriter(s.getOutputStream());
			read = new BufferedReader(new InputStreamReader(s.getInputStream()));
			send = new PrintWriter(s.getOutputStream(), true); 
			os = s.getOutputStream();
			connect();
			

		} catch (Exception e1) {
			log("[Client] Server not online.");
		}
		
	}
	

	/**
	 * Stops the Client Thread and closes the Socket.
	 */
	@Override
	public void stop(){
		
		if(!Listener.isInterrupted()) {
			Listener.interrupt();
		}
		
		if(s != null) {
			log("[Client] Closing Socket!");
			try {
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			log("[Client] Already disconnected.");
		}

	}
	

	/**
	 * Method that accepts messages by the Server sent over the OutputStream
	 * Prints the message to the gui by calling the log() method
	 */
	@Override
	public void waitForMessage() {
		try {
			output = read.readLine(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		log("[Server] " + output);
	}	

	
	/**
	 * Verifies the client connection.
	 * Once the Connection has been built successfully, The Client sends an acknowledged message and starts the Thread Listener.
	 */
	@Override
	public void connect() throws IOException {
		
		if(!Listener.isAlive()) {
			sendMessage("CON"); //send connection request to server
			
			waitForMessage();
			//after connection, check if server returns ACK
			if(Instruction.valueOf(output) ==  Instruction.ACK) {
				//change client socket status to connected
				log("[Client] Connected");
				Listener.start();
				
			}else {
				//if not then the connection wasnt successful, in this case close the socket
				log("[Client] Connection denied");
				//s.close();
			}
		} else {
			log("[Client] Already running");
		}
	}
	
	/**
	 * Waits for an incoming server answer
	 */
	Thread Listener = new Thread() {
	    @Override 
	    public void run() {
			while(true) {
				// Wenn Client Anfrage schickt , dann warte auf antwort
				waitForMessage();
				String instruction = input.split(" ")[0];
				//Hier kommen alle if-anweisungen zu erhaltenen nachrichten hin
				if(Instruction.valueOf(instruction).equals(Instruction.GET)) {
					String filename = input.split(" ")[1];
					if(Instruction.valueOf(output).equals(Instruction.ACK)) {
						sendMessage("ACK");
						waitForMessage();
					}
					
					if(Instruction.valueOf(output.split(" ")[0]).equals(Instruction.DAT)) {
						try {
							Message m = Message.acceptFile(filename, Integer.parseInt(output.split(" ")[1]), s.getInputStream(), true);
							m.open();
						} catch (IOException e) {
							e.printStackTrace();
						}
						sendMessage("ACK");
					}
				}
				
				if(Instruction.valueOf(instruction).equals(Instruction.DEL)){
					
					if(Instruction.valueOf(output).equals(Instruction.ACK)) {
						sendMessage("ACK");
					} 
				}
				
				if(Instruction.valueOf(instruction).equals(Instruction.LST)) {
					if(Instruction.valueOf(output).equals(Instruction.ACK)) {
						sendMessage("ACK");
						waitForMessage();
					}
					
					if(Instruction.valueOf(output).equals(Instruction.DAT)) {
						waitForMessage();
						sendMessage("ACK");
						output = "ACK";
					}
					
				}
				
				if(Instruction.valueOf(instruction).equals(Instruction.PUT)) {
			    	if(Instruction.valueOf(output).equals(Instruction.ACK)) {
				    	sendMessage("DAT "+(int) fileToSend.length());
				    	try {
				    		fileToSend.sendFile(s.getOutputStream());
						} catch (IOException e) {
							e.printStackTrace();
						}
						waitForMessage();
					}
					
				}
				
				if(Instruction.valueOf(output.split(" ")[0]).equals(Instruction.DSC)) {
					try {
						disconnect();
					} catch (Error e) {
						
					}
					
					break;
				}

				input = "ACK";
			}
			try {
				send.close();
				out.close();
				read.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

	    } 
	};
	
	
	/**
	 * Contrary to the connect() method.
	 * Sends a DSC Message and disconnects from The Server.
	 */
	@Override
	public void disconnect(){
		if(this.s != null) {
			sendMessage("DSC");
			log("[Client] Disconnected from Server!");
			stop();
		}
	}

	
	/**
	 * Sends a message (String) to the Server given in the parameter.
	 * @param message : String, a message which is send in raw text and then formatted for client output
	 * This message will be print of the client itself: [Client] Message --> [Server]
	 */
	@Override
	public void sendMessage(String message) {
		
		if(s != null) {
			input = message;
			send.println(message); 
			send.flush();
			log("[Client] "+ message + " --> "+ "[Server]");
		} else {
			log("[Client] Not connected!");
		}
		
	}

	/**
	 *  Logs an action happened on the Client and sends it to the Client GUI.
	 *  @param message : String, sends this data to the {@link pis.hue2.client.gui.GUI#addOutput(String)} method
	 */
	public void log(String message) {
		SwingUtilities.invokeLater(() -> { g.addOutput(message+"\n"); });  
	}
}
