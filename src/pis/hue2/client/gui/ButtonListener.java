package pis.hue2.client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import pis.hue2.client.LaunchClient;
import pis.hue2.common.Message;

/**
 * 
 * Button Listener class using the ActionListener interface
 * Once a button is clicked this class will check for its identity and then perform an action
 */
public class ButtonListener implements ActionListener{

	
	
	
	/**
	 * Each Button does its work when its clicked, depending on the request.
	 * @param e : ActionEvent, gets triggered in the Client GUI whenever a button gets pressed
	 */
	@Override
	
	public void actionPerformed(ActionEvent e) {

		
		if(e.getSource().equals(GUI.con)) {
			new Thread(new Runnable() { 
				public void run() { 
				  LaunchClient.c.start();
			}}).start();
		}
		
		if(e.getSource().equals(GUI.dsc)) {
			LaunchClient.c.disconnect();
		}
		
		if(e.getSource().equals(GUI.lst)) {
			LaunchClient.c.sendMessage("LST");
		}
		
		if(!GUI.argument.getText().equals("") || !GUI.argument.getText().equals("Filename")) {
			if(e.getSource().equals(GUI.get)) {
				LaunchClient.c.sendMessage("GET " + GUI.argument.getText());
			}
			
			if(e.getSource().equals(GUI.del)) {
				LaunchClient.c.sendMessage("DEL " + GUI.argument.getText());
			}
			
			if(e.getSource().equals(GUI.put)) {
				File f = Message.chooseFile();
				LaunchClient.c.fileToSend = new Message(f.getAbsolutePath(), f.getName());
				LaunchClient.c.sendMessage("PUT " + f.getName());
			}
			
		}
	}
}
