package pis.hue2.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import pis.hue2.common.Graphics;

/**
 * 
 * GUI for a client instance 
 */
public class GUI implements Graphics{

	
	// General
	private static JFrame frame = new JFrame();
	private static ButtonListener listener = new ButtonListener();
	public static JTextArea output = new JTextArea();
		
	public static JButton con = new JButton("CONNECT");
	public static JButton dsc = new JButton("DISCONNECT");
	public static JButton get = new JButton("GET");
	public static JButton put = new JButton("PUT");
	public static JButton del = new JButton("DELETE");
	public static JButton lst = new JButton("LIST ALL");
	public static JTextField argument = new JTextField("Filename");

	/**
	 * 
	 * Sets the main frame to visible and calls the init method
	 */
	public GUI(){
		init();
		frame.setVisible(true);
	}
		
	
	/**
	 * 
	 * Initializes all GUI components and places them to their destinated position.
	 */
	@Override	
	public void init() {
			
		frame.setTitle("Client connection");
		frame.setSize(850, 500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
		con.addActionListener(listener);
		dsc.addActionListener(listener);
		get.addActionListener(listener);
		put.addActionListener(listener);
		del.addActionListener(listener);
		lst.addActionListener(listener);
			
		con.setBackground(Color.green);
		con.setForeground(Color.black);
			
		dsc.setBackground(Color.red);
		dsc.setForeground(Color.black);
			
		get.setBackground(Color.blue);
		get.setForeground(Color.black);
			
		put.setBackground(Color.cyan);
		put.setForeground(Color.black);
			
		del.setBackground(Color.MAGENTA);
		del.setForeground(Color.black);
			
		lst.setBackground(Color.yellow);
		lst.setForeground(Color.black);
			
		output.setEditable(false);
			
		output.setBorder(BorderFactory.createLineBorder(Color.black));
		JScrollPane sp = new JScrollPane(output);
		sp.setHorizontalScrollBar(null);
		
		Box buttons = Box.createHorizontalBox();
		buttons.add(Box.createGlue());
		buttons.add(con);
		buttons.add(Box.createHorizontalStrut(10));
		buttons.add(dsc);
		buttons.add(Box.createHorizontalStrut(10));
		buttons.add(get);
		buttons.add(Box.createHorizontalStrut(10));
		buttons.add(put);
		buttons.add(Box.createHorizontalStrut(10));
		buttons.add(del);
		buttons.add(Box.createHorizontalStrut(10));
		buttons.add(lst);
		buttons.add(Box.createHorizontalStrut(10));
		buttons.add(argument);
		buttons.add(Box.createGlue());
		Box.createVerticalStrut(10);
		frame.add(buttons, BorderLayout.SOUTH);
			
		frame.add(sp);
			

			
		frame.addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent e) {
		    	dsc.doClick();
			    	
			 }
		});
			
			
			
		argument.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
					
				if(argument.getText().equals("Filename")) {
					argument.setText("");
				}
			}

			@Override
			public void focusLost(FocusEvent e) {}
				
		});
			
	}
	
	
	/**
	 * 	Here we print out every request and response that is made between a certain Client and the Server on the Client GUI.
	 * @param text : String, that gets added in raw format to the output area
	 */
	public void addOutput(String text) {
		output.append(text);
		output.setCaretPosition(output.getDocument().getLength());
	}
}
