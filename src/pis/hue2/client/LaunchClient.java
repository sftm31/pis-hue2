package pis.hue2.client;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import pis.hue2.client.gui.GUI;

public class LaunchClient{
	
	public static Client c;
	public static GUI g;
	
	public static void main(String[] args) throws InvocationTargetException, InterruptedException{

		
		SwingUtilities.invokeAndWait(() -> { g = new GUI(); });
		
		SwingWorker<?, ?> worker = new SwingWorker<Object, Object>() {
            protected String doInBackground(){
            	c = new Client("localhost",5988,g);
            	return null;
            }
          };
          worker.execute();
		
		 
	}
}
