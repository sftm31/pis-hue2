package pis.hue2.server;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import pis.hue2.server.gui.GUI;

/**
 * 
 * Main-executable class for the server. Instantiates a server object as well as a GUI object running in the EDT and runs them in two threads
 */
public class LaunchServer {
	
	public static Server s;
	public static GUI g;
	
	public static void main(String[] args) throws InvocationTargetException, InterruptedException{

	    SwingUtilities.invokeAndWait(() -> { g = new GUI(); });
		
		SwingWorker<?, ?> worker = new SwingWorker<Object, Object>() {
            protected String doInBackground(){
            	s =new Server(5988,3, g);
              return null;
            }
          };
          worker.execute();
		
		 
	}
}
