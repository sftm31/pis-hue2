package pis.hue2.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.SwingUtilities;

import pis.hue2.common.Message;
import pis.hue2.common.SocketMechanics;
import pis.hue2.server.gui.GUI;

/**
 * 
 * Server object class
 * Creates a ServerSocket and waits for a Socket connection to push it to a new Thread
 * Handles all files on the server  
 */
public class Server implements SocketMechanics{

	/**
	 * The GUI component which the server is working on using the Event Dispatch Thread
	 */
	GUI g;
	
	/**
	 * 
	 * Server socket object on which the connection is running
	 */
	public static ServerSocket server;
	
	/**
	 * 
	 * Awaiting client Socket on which the client will connect
	 */
	public static Socket client;
	
	/**
	 * 
	 * Port of the server  
	 */
	int PORT;
	
	/**
	 * 
	 * Integer which declares the amount of synchronous connections at a time
	 */
	static int maxconnections;
	
	/**
	 * 
	 * List of clients that are connected to the server and verified
	 */
	public static List<Socket> clients = Collections.synchronizedList(new ArrayList<Socket>());
	
	/**
	 * 
	 * List of messages (files) available on the server
	 */
	public static List<Message> messages = Collections.synchronizedList(new ArrayList<Message>());
	
	
	Server(int port, int con, GUI g){
		this.PORT=port;
		maxconnections = con;
		this.g = g;
		
		messages.add(new Message(".\\src\\pis\\hue2\\server\\files\\HelloWorld.txt", "HelloWorld.txt"));
		messages.add(new Message(".\\src\\pis\\hue2\\server\\files\\cat.jpg", "cat.jpg"));
	}
	
	
	/**
	 * 
	 * Starts the server and waits for a socket connection by a client.
	 * Once a socket connected to the server socket the server will push it to a new SocketThread and waits for another socket to connect
	 */
	@Override
	public void start(){
		try {
			server = new ServerSocket(PORT);
			
			log("[Server] Started server");
			log("[Server] Running on "+server.getInetAddress()+":"+server.getLocalPort());
			
			log("[Server] Initializing file system");
			SwingUtilities.invokeLater(() -> { g.showFiles(); });  
			
			while(!server.isClosed()) {
				try {
					client = server.accept();
					
					if(!clients.contains(client)) {
						Thread t = new SocketThread(client, this);
						t.start();
					}
					
					
				} catch (IOException e) {
					if(client != null) {
						
					}
				}
			}
		} catch (IOException e2) {
			log("[Server] Already running");
		}
	}

	/**
	 * 
	 * Stops the server and disconnects all the clients that are connected to it
	 * Sends and DSC to each client from the clients ArrayList
	 */
	@Override
	public void stop() {
		PrintWriter pw;
		
		log("[Server] STOP");
		log("[Server] Disconnecting all clients.");
		
		if(server != null && !server.isClosed()) {
			try {
				if(clients.size() > 0) {
					for(Socket c : clients) {
						pw = new PrintWriter(c.getOutputStream());
						pw.println("DSC");
						pw.flush();
						log("[Server] DSC --> "+ whoisClient(c));
					}
					clients.clear();
					SwingUtilities.invokeLater(() -> { g.showClients(); });  
				}
				server.setSoTimeout(200);
				server.close();
				log("[Server] Stopped server!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			log("[Server] Not running");
		}
	}

	/**
	 *
	 * Logs an action happend on the server and sends it to the server gui
	 * @param message : String, sends this data to the {@link pis.hue2.server.gui.GUI#addOutput(String)} method
	 */
	public void log(String message) {
		SwingUtilities.invokeLater(() -> { g.addOutput(message+"\n"); });  
	}
	
	/**
	 * 
	 * Modified toString() method of a client object
	 * @param c : Socket, gets the port of the socket
	 * @return String, returns an equivalent to the {@link Socket#toString()} method
	 */
	public static String whoisClient(Socket c) {
		return "[Client @"+c.getPort()+"]";
	}
	
	/**
	 * Checks if a Message object with the given parameter String exists
	 * @param t : String, filename of which to filter the array
	 * @return Message, Returns a message object where the filename is equal to the given string parameter
	 * If no suitable message object is found it will return null
	 */
	public Message getMessage(String t) {
		for(Message m : messages) {
			if(m.getName().equals(t)) {
				return m;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * Checks whether a message object with the given string parameter as filename exists
	 * @param t : String, filename of which to filter the array
	 * @return boolean, Returns true if the file was found, returns false if no file was found
	 * 
	 */
	public boolean messageExists(String t) {
		for(Message m : messages) {
			if(m.filename.equals(t)) {
				return true;
			}
		}
		return false;
	}
}
