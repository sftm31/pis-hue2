package pis.hue2.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.SwingUtilities;

import pis.hue2.common.Communicator;
import pis.hue2.common.Instruction;
import pis.hue2.common.Message;
/**
 * A SocketThread is a thread extending class which allows the server to run multiple client connections at the same time 
 * The server is able to communicate with each client on their own 
 */
public class SocketThread extends Thread implements Communicator {
	
	/**
	 * The server the socket thread is handling the client connection on
	 */
	Server server;
	
	/**
	 * 
	 * Client which is handled on the thread
	 */
	Socket client;
	
	/**
	 * String for the output by the client
	 */
	String output;
	
	/**
	 * Catches the input stream of the server 
	 */
	InputStreamReader in;
	
	/**
	 * Reads a string message out of the input stream
	 * Waits for a message
	 */
	BufferedReader br;
	
	/**
	 * Writes messages to the client using the output stream
	 */
	PrintWriter pw;
	
	SocketThread(Socket clientOnSocket, Server s){
		this.client = clientOnSocket;
		this.server = s;
	}

	/**
	 * Runs the thread and waits for a request by the client
	 */
	@Override
	public void run() {		
		try {
			in = new InputStreamReader(client.getInputStream());
			br = new BufferedReader(in);
			pw = new PrintWriter(client.getOutputStream());
			
			
			while(!client.isClosed()) {
				waitForMessage();
				
				if(output != null) {
					
					String instruction = output.split(" ")[0];
					
					
					if(Instruction.valueOf(instruction) == Instruction.CON) {
						if(Server.clients.size() < Server.maxconnections) {
							sendMessage("ACK");
							connect();
						} else {
							server.log(Server.whoisClient(client)+" refused connection.");
							sendMessage("DND");
							closeClient();
							break;
						}
					}
					
				    if(Instruction.valueOf(instruction) == Instruction.DSC) {
				    	try {
				    		disconnect();
				    	} catch (IndexOutOfBoundsException e){
				    		
				    	}
				    	break;
				   	}	
				    
				    if(Instruction.valueOf(instruction) == Instruction.GET) {
				    	String filename = output.split(" ")[1];
				    	if(server.messageExists(filename)) {
				    		sendMessage("ACK");
							waitForMessage();
					    	if(Instruction.valueOf(output) == Instruction.ACK) {
					    		sendMessage("DAT "+(int) server.getMessage(filename).length());
					    		server.getMessage(filename).sendFile(client.getOutputStream());
								waitForMessage();
								
								if(Instruction.valueOf(output) == Instruction.ACK) {
									server.log("[Server] File transfer successful");
								}
					    	} else {
					    		sendMessage("DND");
					    	}
				    	} else {
				    		sendMessage("DND");
				    	}
				    	
				   	} 
				    
				    if(Instruction.valueOf(instruction) == Instruction.DEL) {
				    	if(server.messageExists(output.split(" ")[1])) {
				    		Server.messages.remove(server.getMessage(output.split(" ")[1]));
				    		SwingUtilities.invokeLater(() -> { server.g.showFiles(); });  
				    		sendMessage("ACK");
				    	} else {
				    		sendMessage("DND");
				    	}
				   	}
				    
				    if(Instruction.valueOf(instruction) == Instruction.PUT) {
				    	String filename = output.split(" ")[1];
				    	sendMessage("ACK");
						waitForMessage();
						
						if(Instruction.valueOf(output.split(" ")[0]).equals(Instruction.DAT)) {
							
							Message m = Message.acceptFile("Client@"+client.getPort()+"@"+filename, Integer.parseInt(output.split(" ")[1]), client.getInputStream(), false);
							m.open();
							Server.messages.add(m);
							SwingUtilities.invokeLater(() -> { server.g.showFiles(); });  
							sendMessage("ACK");
						} else {
							sendMessage("DND");
						}
				   	}
				    
				    if(Instruction.valueOf(instruction) == Instruction.LST) {
				    	sendMessage("ACK");
						waitForMessage();
				    	if(Instruction.valueOf(output) == Instruction.ACK) {
				    		sendMessage("DAT");
				    		String s = "";
				    		for(Message m : Server.messages) {
				    			s += m.filename+", ";
				    		}
				    		sendMessage("Available messages: "+s);
				    	}
				    	
				   	}
				}

			}
			closeClient();
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
				pw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * Method that accepts messages by the client sent over the InputStream
	 * Prints the message to the gui by calling the log() method
	 */
	@Override
	public void waitForMessage() {
		try {
			output = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(output != null) {
			server.log(Server.whoisClient(client)+" "+ output);
		}
		
	}
	
	/**
	 * 
	 * Verifies the client connection
	 * As the socket connection is valid by just connecting, the server wants to make sure that the maximum connections dont get exceeded
	 * Once the verification was executed this method adds the client to the clients ArrayList
	 */
	@Override
	public void connect() throws IOException {
		server.log(Server.whoisClient(client)+" connected.");
		Server.clients.add(client);
		SwingUtilities.invokeLater(() -> { server.g.showClients(); });  
	}

	
	/**
	 * 
	 * Contrary to the connect() method
	 * Sends a DSC message to the client and then removes the client from the ArrayList and calls the closeClient() method
	 */
	@Override
	public void disconnect() {
		sendMessage("DSC");
		Server.clients.remove(Server.clients.indexOf(client));
		SwingUtilities.invokeLater(() -> { server.g.showClients(); });  
		closeClient();
		server.log(Server.whoisClient(client)+" disconnected.");
	}
	
	
	/**
	 * 
	 * Sends a message (String) to the client
	 * @param message : String, a message which is send in raw text and then formatted for server output
	 * This message will be print of the server itself: [Server] Message --> [Client@ID]
	 */
	@Override
	public void sendMessage(String message) {
		pw.println(message);
		pw.flush();
		
		server.log("[Server] "+ message + " --> "+ Server.whoisClient(client));
	}
	
	/**
	 * 
	 * Closes the client and ends the stream
	 */
	public void closeClient() {
		try {
			this.client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
