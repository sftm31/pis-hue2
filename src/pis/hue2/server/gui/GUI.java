package pis.hue2.server.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import pis.hue2.common.Graphics;
import pis.hue2.common.Message;
import pis.hue2.server.Server;

/**
 * Graphical display of the server
 * It has an output field, as well as a start and a stop button
 * Connected clients and available server files are also displayed when the server is running
 */
public class GUI implements Graphics{

	// General
	private static JFrame frame = new JFrame();
	private static JTextArea output = new JTextArea(40,40);
	private static JTextArea clientList = new JTextArea(5,5);
	private static JTextArea fileList = new JTextArea(1,5);
	public static JButton start = new JButton("Start");
	public static JButton stop = new JButton("Stop ");
	ButtonListener b = new ButtonListener();

	/**
	 * 
	 * Sets the main frame to visible and calls the init method
	 */
	public GUI(){
		init();
		frame.setVisible(true);
	}
	
	/**
	 * Initializes all GUI components and places them to their destinated position
	 */
	@Override
	public void init() {
		// Frame 
		frame.setTitle("Server connection");
		frame.setSize(900, 1000);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Box buttons = Box.createVerticalBox();
		buttons.add(Box.createVerticalStrut(40));
		buttons.add(start);
		buttons.add(Box.createVerticalStrut(10));
		buttons.add(stop);
		buttons.setSize(100, 1000);
		frame.add(buttons, BorderLayout.WEST);
		
		Box clients = Box.createVerticalBox();
		clients.add(new JLabel("Connected clients:"));
		clients.add(clientList);
		clients.add(Box.createGlue());
		frame.add(clients, BorderLayout.NORTH);
		
		Box file = Box.createHorizontalBox();
		file.add(new JLabel("Files on server:"));
		file.add(Box.createVerticalStrut(20));
		file.add(fileList);
		frame.add(file, BorderLayout.SOUTH);
		
		start.addActionListener(b);
		stop.addActionListener(b);
		
		start.setBackground(Color.black);
		start.setForeground(Color.white);
		stop.setBackground(Color.BLUE);
		stop.setForeground(Color.WHITE);
		
		output.setBorder(BorderFactory.createLineBorder(Color.black));
		
		output.setEditable(false);
		clientList.setEditable(false);
		fileList.setEditable(false);
		
		clientList.setText("No clients connected");
		JScrollPane sp = new JScrollPane(output);
		sp.setHorizontalScrollBar(null);

		frame.add(sp);
		
		frame.addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent e) {
		    	
		    	stop.doClick();
		    	
		    }
		});
	}
	
	/**
	 * 
	 * Called by the ServerThread, manages the server output field
	 * @param text : String, gets added to the output area
	 */
	public void addOutput(String text) {
		output.append(text);
		output.setCaretPosition(output.getDocument().getLength());
	}
	
	/**
	 * 
	 * Called by the Server, shows all available files and adds/removes them from the area
	 */
	public void showFiles() {
		fileList.setText("");
		
		for(Message m : Server.messages) {
			fileList.append(m.filename+", ");
		}
		
	}
	
	/**
	 * 
	 * Called by the ServerThread, add a client to the text area if the connection was successful
	 * Removes the client from the list, if the client disconnects or the server closes
	 */
	public void showClients() {
		clientList.setText("");
		for(Socket s : Server.clients) {
			clientList.append(Server.whoisClient(s)+"\n");
		}
	}

}


