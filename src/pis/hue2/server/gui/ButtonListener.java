package pis.hue2.server.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import pis.hue2.server.LaunchServer;

/**
 * 
 * ButtonListener for the start and the stop button
 * Checks which button was pressed and runs the action in the LaunchServer class
 */
public class ButtonListener implements ActionListener{

	/**
	 * Performs an action for the start and the stop button accessing the {@link pis.hue2.server.LaunchServer#s} object
	 * @param e : ActionEvent, gets triggered in the Client GUI whenever a button gets pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource().equals(GUI.start)) {
			new Thread(new Runnable() { 
				public void run() { 
				  LaunchServer.s.start();
			}}).start();
			
		} else {
			LaunchServer.s.stop();
		}
		
	}

}
