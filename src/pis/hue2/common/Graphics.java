package pis.hue2.common;

/**
 * 
 * Structure which a client and a server GUI is following
 */
public interface Graphics {
	
	/**
	 * Initializes Frame Objects like Labels, Buttons, etc.
	 */
	public void init();
	
	/**
	 * Adds output message to the gui console window
	 * @param text : String, gets added to the output area in the guy
	 */
	public void addOutput(String text);
	
}
