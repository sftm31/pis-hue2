package pis.hue2.common;

import java.io.IOException;

/**
 * 
 * Mechanics that each socket can perform in runtime
 *
 */
public interface SocketMechanics {

	/**
	 * Starts the socket and tries to connect to the destinated port
	 * @throws for java.io.IOException
	 */
	public void start() throws IOException;
	
	/**
	 * Closes the socket
	 * @throws for java.io.IOException
	 */
	public void stop() throws IOException;
}
