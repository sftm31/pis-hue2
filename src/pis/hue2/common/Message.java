package pis.hue2.common;

import java.awt.Desktop;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

/**
 * 
 * Object that extends the File object 
 * Has an additional open() method, which will open the file to the certain instance that called the method
 */
public class Message extends File{
	
	/**
	 * Filename of the file
	 */
	public String path;
	public String filename;
	
	private static final long serialVersionUID = -9033547865007459466L;
	
	static JFileChooser chooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
	
	/**
	 * 
	 * Constructor that calls the super constructor
	 * @param filename : String, sets the filename of the file 
	 */
	public Message(String path, String filename){
		super(path);
		this.path = path;
		this.filename = filename;
	}
	
	/**
	 * Opens the object with which the method is called.
	 * A desktop object is used for that.
	 */
	public void open() {
		Desktop desktop = Desktop.getDesktop();      
		try {
			desktop.open(this);
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public static File chooseFile(){
		
		int returnValue = chooser.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile();
        }
		return null;
	}
	
	/**
	 * Sends an existing file to the receiver 
	 * @param os : OutputStream on which the byte buffer is written on
	 */
	public void sendFile(OutputStream os) {
		
		byte [] bytes  = new byte [(int)this.length()];
		 
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(this));
             
            bis.read(bytes, 0, bytes.length);
            os.write(bytes, 0, bytes.length);
            os.flush();
            
            bis.close();
            
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Waits for the file sent by the sender in sendFile()
	 * @param title : String, the title of the file as which its supposed to be saved in the project directory
	 * @param length : int, the size of bytes needed to be transmitted
	 * @param is : InputStream stream on which the byte buffer gets received
	 */
	public static Message acceptFile(String title, int length, InputStream is, boolean client) {
		Message m;
		
		if(client) {
			chooser.setDialogTitle("Choose a directory to save the file: ");
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.showSaveDialog(null);
			m = new Message(chooser.getSelectedFile()+"\\"+title, title);
		} else {
			m = new Message(".\\src\\pis\\hue2\\server\\files\\"+title, title);
		}
		
		byte[] bytes = new byte[length];

		try {     

	        FileOutputStream fos = new FileOutputStream(m);
	        BufferedOutputStream bos = new BufferedOutputStream(fos);
	         
	        int file = is.read(bytes, 0, bytes.length);
	        bos.write(bytes, 0, file);
	        bos.close();
	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return m;
	}
	
	
}
