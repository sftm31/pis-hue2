package pis.hue2.common;

import java.io.IOException;


/**
 * 
 * Communication framework which allows a client or server instance to perform certain actions while using the instruction interface
 *
 */
public interface Communicator {

	/**
	 * Waits for a message to be received
	 */
	public void waitForMessage();
	
	/**
	 * Establishes a connection to a socket with the given port
	 */
	public void connect() throws IOException;
	
	/**
	 * Closes the socket connection
	 */
	public void disconnect() throws IOException;
	
	/**
	 * Detects from the given message, which enum type is used and then sends the message in the correct syntactical way to the receiver
	 * @param message : String, an enum of {@link pis.hue2.common.Instruction} additionally encapsulated with an argument (BYTE_SIZE/FILENAME/LIST)
	 */
	public void sendMessage(String message);

}
